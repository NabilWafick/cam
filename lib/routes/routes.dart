// ignore_for_file: unused_local_variable

import 'package:Cam/views/screen_cam/camera.dart';
import 'package:Cam/views/screen_cam/simple_cam.dart';
import 'package:Cam/views/screen_home/home.dart';
import 'package:flutter/material.dart';

class RoutesManager {
  static const String home = '/';
  static const String simpleCam = '/simpleCam';
  static const String cam = '/cam';

  static Route onGenerate(RouteSettings routeSettings) {
    dynamic passedValue;

    if (routeSettings.arguments != null) {
      passedValue = routeSettings.arguments as Map<String, dynamic>;
    }

    switch (routeSettings.name) {
      case home:
        return MaterialPageRoute(
          builder: (context) => const Home(),
        );

      case simpleCam:
        return MaterialPageRoute(
          builder: (context) => const SimpleCamera(),
        );
      case cam:
        return MaterialPageRoute(
          builder: (context) => const Camera(),
        );

      default:
        throw (Exception('That route doesn\'t exist'));
    }
  }
}
