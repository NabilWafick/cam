import 'package:Cam/routes/routes.dart';
import 'package:Cam/views/screen_home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: RoutesManager.home,
      onGenerateRoute: RoutesManager.onGenerate,
      home: const Home(),
    );
  }
}
