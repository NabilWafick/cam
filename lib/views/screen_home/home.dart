import 'package:Cam/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class Home extends ConsumerWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () =>
                    Navigator.of(context).pushNamed(RoutesManager.simpleCam),
                child: const Text(
                  'Open Simple Camera',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                )),
            const SizedBox(
              height: 15.0,
            ),
            ElevatedButton(
                onPressed: () =>
                    Navigator.of(context).pushNamed(RoutesManager.cam),
                child: const Text(
                  'Open Camera with Face Detection',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                )),
          ],
        ),
      )),
    );
  }
}
