import 'package:better_open_file/better_open_file.dart';
import 'package:camerawesome/camerawesome_plugin.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:path_provider/path_provider.dart';

class SimpleCamera extends ConsumerWidget {
  const SimpleCamera({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: SafeArea(
        child: CameraAwesomeBuilder.awesome(
          saveConfig: SaveConfig.photoAndVideo(
            photoPathBuilder: () => _path(CaptureMode.photo),
            videoPathBuilder: () => _path(CaptureMode.video),
            initialCaptureMode: CaptureMode.photo,
          ),
          sensor: Sensors.front,
          filter: AwesomeFilter.None,
          flashMode: FlashMode.none,
          aspectRatio: CameraAspectRatios.ratio_16_9,
          previewFit: CameraPreviewFit.fitWidth,
          onMediaTap: (mediaCapture) {
            OpenFile.open(mediaCapture.filePath);
          },
        ),
      ),
    );
  }

  Future<String> _path(CaptureMode captureMode) async {
    final dir = await getApplicationDocumentsDirectory();
    final camDir = await dir.create(recursive: true);
    final String fileExtension =
        captureMode == CaptureMode.photo ? 'jpg' : 'mp4';
    final String filePath =
        '${camDir.path}/${DateTime.now().millisecondsSinceEpoch}.$fileExtension';
    return filePath;
  }
}
